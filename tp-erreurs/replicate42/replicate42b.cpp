#include <iostream>
#include <exception>
#include <vector>

enum class NumError { TooLow, TooHigh, Is37, IsNegative };

// Reads a number in stdin. Throws a NumError if 37 or negative.
int readPositiveButNot37() {
    std::string line;
    std::cout << "enter num: ";
    std::flush(std::cout);
    std::getline(std::cin, line);
    int num = stod(line);
    if (num<0) {
        throw NumError::IsNegative;
    }
    if (num==37) {
        throw NumError::Is37;
    }
    
    
    return num;
}

// Reads a number in stdin. Throws a NumError if too low or too high.
std::vector<int> replicate42(int n) {
    if (n==0) {
        throw NumError::TooLow;
    }
    if (n>42) {
        throw NumError::TooHigh;
    }
    
    return std::vector<int>(n, 42);
}

int main() {
    try
    {
        // read number
        const int num = readPositiveButNot37();
        std::cout << "num = " << num << std::endl;

        // build vector
        const auto v = replicate42(num);

        // print vector
        for (const int x : v)
            std::cout << x << " ";
        std::cout << std::endl;        
    }
    catch(NumError e)
    {
        switch (e)
        {
            case NumError::Is37:
                std::cout << "error : 37" << std::endl;
                exit(-1);
                break;
            
            case NumError::IsNegative:
                std::cout << "error : negative" << std::endl;
                exit(-1);
                break;
            
            case NumError::TooHigh:
                std::cout << "error : too high" << std::endl;
                exit(-1);
                break;

            case NumError::TooLow:
                std::cout << "error : too low" << std::endl;
                exit(-1);
                break;
        
            default:
                break;
        }
    }
    


    return 0;
}

