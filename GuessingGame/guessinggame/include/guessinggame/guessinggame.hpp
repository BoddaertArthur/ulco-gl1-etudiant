#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <iostream>

using namespace std;
/// Implementation of the guess game
class Game
{
    public:
        /// \brief Constructeur
        ///
        /// Choisit un nombre aléatoire entre 1 et 100 faisant office de réponse et initialise le nombre d'essais restants à 5.
        Game();

        /// \brief Le nombre que le joueur doit deviner.
        unsigned int answer;    
        /// \brief Le nombre d'essais restants.
        unsigned int remaining_guess;

        /// \brief Destructeur
        ///
        /// Ne fait rien en l'état.
        ~Game();
        /// \brief Setter pour l'attribut remaining_guess
        ///
        /// Change la valeur de l'attribut remaining_guess
        void setGuesses(unsigned int g);
        /// \brief
        ///
        /// Lance le jeu et demande au joueur unnombre jusqu'à sa victoire ou sa défaite.
        unsigned int guessing_game(int guess = 0);
};
