#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

class Game
{
    public:
        Game();

        unsigned int answer;
        unsigned int remaining_guess;

        ~Game();
        void setGuesses(unsigned int g);
        unsigned int guessing_game(int guess = 0);
};
