#include <guessinggame/guessinggame.hpp>

#include <stdio.h>

Game::Game()
{
    srand((unsigned int)time(0));
    answer = rand()%100;
    remaining_guess = 5;
}

Game::~Game()
{}

void Game::setGuesses(unsigned int g)
{
    remaining_guess = g;
}

unsigned int Game::guessing_game(int guess)
{
    if (guess < 0) {
        cout << "C'est un nombre négatif..." << endl;
        return 0;
    }
    if (guess > 100) {
        cout << "C'est un supérieur à 100..." << endl;
        return 0;
    }

    if (remaining_guess <= 0) {
        cout << "Perdu!" << endl;
        return 0;
    }
    if (guess == answer) {
            cout << "Bien joué!" << endl;
            return 1;
    }else {
        if (remaining_guess != 5) {
            if (guess > answer)
                cout << "Trop haut!" << endl;
            if (guess < answer)
                cout << "Trop bas!" << endl;            
        }
    }
    if (remaining_guess > 0) {
        cout << "Entrez un nombre : ";
        int newGuess;
        cin >> newGuess;
        cout << endl;
        remaining_guess = remaining_guess - 1;
        return guessing_game(newGuess);
    }
}

