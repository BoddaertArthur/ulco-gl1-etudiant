/// \mainpage Guess game Documentation
///
/// The goal is to guess a target value, randomly choosen between 1 and 100. We have 5 tries to guess the value. \n
/// When playing a value, the game returns if the value is too low or too high, or if the game is won or lost.
