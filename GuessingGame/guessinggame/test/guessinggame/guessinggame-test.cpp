#include <guessinggame/guessinggame.hpp>

#include <catch2/catch.hpp>

TEST_CASE( "test 1" ) {
    Game g;
    unsigned int a = g.answer;
    g.setGuesses(0);
    REQUIRE( g.guessing_game(a+1) == 0 );
}

TEST_CASE( "test 2" ) {
    Game g;
    unsigned int a = g.answer;
    REQUIRE( g.guessing_game(a) == 1 );
}

TEST_CASE( "test 3" ) {
    Game g;
    unsigned int a = -5;
    REQUIRE( g.guessing_game(a) == 0 );
}

TEST_CASE( "test 4" ) {
    Game g;
    unsigned int a = 150;
    REQUIRE( g.guessing_game(a) == 0 );
}

