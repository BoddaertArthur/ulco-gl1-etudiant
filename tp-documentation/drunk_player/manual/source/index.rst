.. drunk_player documentation master file, created by
   sphinx-quickstart on Mon Nov  9 12:21:21 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentation de drunk_player
=============================

Résumé
------

Drunk_player est un système de lecture de vidéos qui a trop bu.

Drunk_player utilise la bibliothèque de traitement d'image OpenCV et est composé :

- d'une bibliothèque contenant le code de base
- d'un programme graphique qui affiche le résultat à l'écran
- d'un programme console qui sort le résultat dans un fichier

Sommaire
--------

