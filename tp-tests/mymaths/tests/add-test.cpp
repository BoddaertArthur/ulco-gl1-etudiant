#include <mymaths/mymaths.hpp>

#include <catch2/catch.hpp>

TEST_CASE("on teste add2") {
    REQUIRE( add2(0) == 2 );
    REQUIRE( add2(-2) == 0 );
}

TEST_CASE("on teste addn") {
    REQUIRE( addn(0, 2) == 2 );
    REQUIRE( addn(-2, 2) == 0 );
}