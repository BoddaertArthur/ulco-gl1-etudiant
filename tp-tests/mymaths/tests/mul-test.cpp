#include <mymaths/mymaths.hpp>

#include <catch2/catch.hpp>

TEST_CASE("on teste add2") {
    REQUIRE( mul2(0) == 0 );
    REQUIRE( mul2(-2) == -4 );
}

TEST_CASE("on teste addn") {
    REQUIRE( muln(0, 2) == 0 );
    REQUIRE( muln(-2, 2) == 4 );
}