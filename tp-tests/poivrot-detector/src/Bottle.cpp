#include "Bottle.hpp"

#include <sstream>

double alcoholVolume(const Bottle & b) {
    return b._vol * b._deg;
}

double alcoholVolume(const std::vector<Bottle> & bs) {
    double res = 0;
    for (int i = 0; i < bs.size(); ++i) {
        res += alcoholVolume(bs[i]);
    }
    return res;
}

bool isPoivrot(const std::vector<Bottle> & bs) {
    if (alcoholVolume(bs) > 0.1) {
        return true;
    }
    return false;
}

std::vector<Bottle> readBottles(std::istream & is) {
    /*
    std::string line;
    int i=0;
    while(std::getline(is, line)){
        i+=1;
    }
    std::vector<Bottle> bouteilles(i);
    while(std::getline(is, line)){
        bouteilles[i] = line;
    }
    return bouteilles;
    */
}

