#include "../src/Bottle.hpp"

#include <catch2/catch.hpp>
#include <sstream>

TEST_CASE( "init Chimay" ) {
    const Bottle b {"Chimay", 0.75, 0.08};
    REQUIRE(b._name == "Chimay");
    REQUIRE(b._vol == 0.75);
    REQUIRE(b._deg == 0.08);
}

TEST_CASE( "init Orange Pur Jus" ) {
    const Bottle b {"Orange Pur Jus", 1, 0};
    REQUIRE(b._name == "Orange Pur Jus");
    REQUIRE(b._vol == 1);
    REQUIRE(b._deg == 0);
}

TEST_CASE( "alcoholVolume Chimay" ) {
    Bottle b {"Chimay", 0.75, 0.08};
    double res = alcoholVolume(b);
    REQUIRE( res == 0.06 );
}

TEST_CASE( "alcoholVolume Orange Pur Jus" ) {
    Bottle b {"Orange Pur Jus", 1, 0};
    double res = alcoholVolume(b);
    REQUIRE( res == 0 );
}

TEST_CASE( "alcoholVolume Chimay + Orange" ) {
    Bottle b1 {"Chimay", 0.75, 0.08};
    Bottle b2 {"Orange Pur Jus", 1, 0};
    std::vector<Bottle> bouteilles(2);
    bouteilles[0] = b1;
    bouteilles[1] = b2;
    double res = alcoholVolume(bouteilles);
    REQUIRE( res == 0.06 );
}

TEST_CASE( "alcoholVolume Chimay + Chimay" ) {
    Bottle b1 {"Chimay", 0.75, 0.08};
    Bottle b2 {"Chimay", 0.75, 0.08};
    std::vector<Bottle> bouteilles(2);
    bouteilles[0] = b1;
    bouteilles[1] = b2;
    double res = alcoholVolume(bouteilles);
    REQUIRE( res == 0.12 );
}

TEST_CASE( "isPoivrot Chimay + Orange" ) {
    Bottle b1 {"Chimay", 0.75, 0.08};
    Bottle b2 {"Orange Pur Jus", 1, 0};
    std::vector<Bottle> bouteilles(2);
    bouteilles[0] = b1;
    bouteilles[1] = b2;
    REQUIRE( isPoivrot(bouteilles) == false );
}

TEST_CASE( "isPoivrot Chimay + Chimay" ) {
    Bottle b1 {"Chimay", 0.75, 0.08};
    Bottle b2 {"Chimay", 0.75, 0.08};
    std::vector<Bottle> bouteilles(2);
    bouteilles[0] = b1;
    bouteilles[1] = b2;
    REQUIRE( isPoivrot(bouteilles) == true );
}

TEST_CASE( "readBottles Chimay" ) {
    // TODO
}

TEST_CASE( "readBottles Chimay + Orange" ) {
    // TODO
}

