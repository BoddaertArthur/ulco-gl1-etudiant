#include "MyClass.hpp"

#include <catch2/catch.hpp>
#include <sstream>

TEST_CASE( "init & getter", "[MyClass]" ) {
    MyClass ma_classe;
    REQUIRE( ma_classe.mydata() == "" );
}

TEST_CASE( "setter", "[MyClass]" ) {
    MyClass ma_classe;
    ma_classe.mydata() = "my data";
    REQUIRE( ma_classe.mydata() == "my data" );
}

TEST_CASE( "reset", "[MyClass]" ) {
    MyClass ma_classe;
    ma_classe.mydata() = "my data";
    ma_classe.reset();
    REQUIRE( ma_classe.mydata() == "" );
}

TEST_CASE( "fail", "[MyClass]" ) {
    MyClass ma_classe;
    try
    {
        ma_classe.fail();
        REQUIRE( false );
    }
    catch(const std::exception& e)
    {
        REQUIRE( true );
    }
    
}

TEST_CASE( "sqrt2", "[MyClass]" ) {
    MyClass ma_classe;
    double epsilon = 0.001;
    double sqrt = ma_classe.sqrt2();
    REQUIRE( sqrt < 1.414 + epsilon );
    REQUIRE( sqrt > 1.141 - epsilon );
}

TEST_CASE( "operator<<", "[MyClass]" ) {
    
}

