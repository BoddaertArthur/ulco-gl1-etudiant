#pragma once

#include <string>
#include <iostream>

using namespace std;

class Cesar
{
    public:
        Cesar(string myData);
        ~Cesar();

        unsigned int n;
        string data;

        void setN(unsigned int k);
        char decaler(char c);
        string chiffrer();
        char decaler2(char c, int k);
        string dechiffrer(string s, int k);
};

