#include <cesar/cesar.hpp>

#include <catch2/catch.hpp>

TEST_CASE( "test decaler()" ) {
    Cesar c("test");
    c.setN(1);
    REQUIRE( c.decaler('a') == 'b' );
    REQUIRE( c.decaler('z') == 'a' );
}

TEST_CASE( "test chiffrer()" ) {
    Cesar c("test");
    c.setN(1);
    REQUIRE( c.chiffrer() == "uftu" );
}

TEST_CASE( "test decaler2()" ) {
    Cesar c("test");
    REQUIRE( c.decaler2('a', 1) == 'z' );
    REQUIRE( c.decaler2('z', 1) == 'y' );
    REQUIRE( c.decaler2('u', 1) == 't' );
    REQUIRE( c.decaler2('f', 1) == 'e' );
}

TEST_CASE( "test dechiffrer()" ) {
    Cesar c("uftu");
    REQUIRE( c.dechiffrer("uftu", 1) == "test" );
}