#include <cesar/cesar.hpp>

#include <iostream>

int main() {
    std::string str;
    std::string line;
    while(std::getline(std::cin, line)){
        str+=line+"\n";
    }
    std::cout << str << std::endl;
    Cesar c(str);
    std::cout << "key = " << c.n << std::endl;
    return 0;
}

