#include <cesar/cesar.hpp>

Cesar::Cesar(string myData) {
    srand((unsigned int)time(0));
    n = (rand()%26)+1;
    data = myData;
}

Cesar::~Cesar() {}

void Cesar::setN(unsigned int k) {
    n = k;
}

char Cesar::decaler(char c) {
    if (c != ' ' && c != '.' && c != ',') {
        if ((int)(c+n) > (int)'x')
            return (char)('a'+((int)(122-c)%26));
        return (char)(c+n);    
    }
    return c;
}

string Cesar::chiffrer() {
    string str;
    for (char& c : data) {
        str += decaler(c);
    }
    return str;
}

char Cesar::decaler2(char c, int k) {
    if (c != ' ' && c != '.' && c != ',') {
        if((int)(c-k) < (int)'a')
            return (char)('z'-(int)(97-c)%26);
        return (char)(c-k);
    }
    return c;
}

string Cesar::dechiffrer(string s, int k) {
    string str;
    for (char& c : s) {
        str += decaler2(c, k);
    }
    return str;
}