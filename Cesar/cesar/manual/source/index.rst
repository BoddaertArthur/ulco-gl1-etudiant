.. cesar documentation master file, created by
   sphinx-quickstart on Thu Dec  3 15:32:28 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to cesar's documentation!
=================================

Description
-----------

`Cesar cipher <https://fr.wikipedia.org/wiki/Chiffrement_par_d%C3%A9calage>`_ is one of the simplest and most widely known
encryption techniques. It is a type of substitution cipher in which
each letter in the plaintext is replaced by a letter some fixed number
of positions down the alphabet. For example, with a left shift of 3, D
would be replaced by A, E would become B, and so on.

.. image:: _static/cesar.jpeg

Usage
-----

* write the encrypted message in a file:
   
   crkzqflkxi moldoxjjfkd fp pl zxiiba ybzxrpb x moldoxj zlkpfpqp bkqfobiv lc crkzqflkp. qeb jxfk moldoxj fqpbic fp tofqqbk xp x crkzqflk tefze obzbfsbp qeb moldoxj’p fkmrq xp fqp xodrjbkq xka abifsbop qeb moldoxj’p lrqmrq xp fqp obpriq. qvmfzxiiv qeb jxfk crkzqflk fp abcfkba fk qbojp lc lqebo crkzqflkp, tefze fk qrok xob abcfkba fk qbojp lc pqfii jlob crkzqflkp rkqfi xq qeb ylqqlj ibsbi qeb crkzqflkp xob ixkdrxdb mofjfqfsbp.

* run caesar: ::

   $ ./build/cesar < input.txt > output.txt

* display the decrypted message:




Attack
------

   Brute-force attack: take one key k in [0; 25], crypt the message using k, compute the letter frequences f and compare to the reference frequencies f using:

Finally , take the key which minimizes e;